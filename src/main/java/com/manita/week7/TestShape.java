package com.manita.week7;

public class TestShape {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(10, 5);
        rect1.PrintAreaRect1();
        rect1.PrintPrimeterRect1();
        Rectangle rect2 = new Rectangle(5, 3);
        rect2.PrintAreaRect2();
        rect2.PrintPrimeterRect2();
        Circle circle1 = new Circle(1);
        circle1.PrintAreaCircle1();
        circle1.PrintPrimeterCircle1();
        Circle circle2 = new Circle(2);
        circle2.PrintAreaCircle2();
        circle2.PrintPrimeterCircle2();
        Triangle triangle1 = new Triangle(5, 5, 6);
        triangle1.PrintAreaTriangle();
        triangle1.PrintPrimeterTriangle();
    }
    
}
