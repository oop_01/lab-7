package com.manita.week7;

public class Circle {
    private double radius;
    private double area;
    private double primeter;
    
    public Circle(double radius) {
        this.radius = radius;
    }
    public void PrintAreaCircle1() {
        area = Math.PI * Math.pow(radius, 2);
        System.out.println("AreaCircle1 = "+ area);
    }
    public void PrintPrimeterCircle1() {
        primeter = 2 * Math.PI * radius;
        System.out.println("PrimeterCircle1 = "+ primeter);
    }
    public void PrintAreaCircle2() {
        area = Math.PI * Math.pow(radius, 2);
        System.out.println("AreaCircle2 = "+ area);
    }
    public void PrintPrimeterCircle2() {
        primeter = 2 * Math.PI * radius;
        System.out.println("PrimeterCircle2 = "+ primeter);
    }
    public double getradius() {
        return radius;
    }
    public double getarea() {
        return area;
    }
    public double getprimeter() {
        return primeter;
    }
}
